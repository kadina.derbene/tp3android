package com.example.uapv1900482.tp3;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    String text,country;
    WeatherDbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);
        db=new WeatherDbHelper(this);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                text = textName.getText().toString();
                country = textCountry.getText().toString();
                City city=new City( text,country);

                db.addCity(city);
                openDialogue();
                Toast.makeText(NewCityActivity.this,"0",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(NewCityActivity.this, MainActivity.class);
                startActivity(intent);





            }
        });
    }
    public void openDialogue(){
        Dialogue dialogue=new Dialogue();
        dialogue.show(getSupportFragmentManager(),"response");
    }


}
