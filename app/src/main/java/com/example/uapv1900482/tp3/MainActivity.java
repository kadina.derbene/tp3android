package com.example.uapv1900482.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    WeatherDbHelper db;
    TextView text1, text2;
    SimpleCursorAdapter cursor;
    ListView Liste;
    private int refreche = 0;
    private SwipeRefreshLayout refrechLayout;
    JSONResponseHandler json;
    InputStream is;
    InputStreamReader reader;
    City city;
    private static final String TAG ="MainActivity";


    ArrayList<City> lesCity;

    String name, country, temperature, speed, couldiness, update, humidity, direction, icon, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);


                startActivityForResult(intent, 1);

                getAllData();





            }
        });



        //*******************declaration de variables********************************
        db = new WeatherDbHelper(this);

        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        Liste = (ListView) findViewById(R.id.liste);
        refrechLayout = findViewById(R.id.swiperefresh);


        db.populate();
        getAllData();
        lesCity = this.liste();



        Liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, CityActivity.class);

                Cursor corsor = db.fetchAllCities();
                corsor.moveToPosition(position);
                city = db.cursorToCity(corsor);
                intent.putExtra("city", city);
                getAllData();
                startActivity(intent);



            }
        });

        ////////////create sipe menu

        //refrech layout

        refrechLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

            findweather();
            getAllData();
            }
        });


    }
    public void findweather(){

         try {
            Cursor corsor = db.fetchAllCities();
            while(corsor.moveToNext()) {
                city = db.cursorToCity(corsor);
                URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                ExecuteTask1 task = new ExecuteTask1();

                task.execute(url.toString());
                Toast.makeText(MainActivity.this, city.getName() + "Ville Ajoutée", Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this, url.toString() + "Ville Ajoutée", Toast.LENGTH_LONG).show();


            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }
    public void openDialogue(){
        Dialogue dialogue=new Dialogue();
        dialogue.show(getSupportFragmentManager(),"reponse");
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //************************fonction qui recupére les données de la base de données*********************


    public void getAllData() {
        Cursor cur = db.fetchAllCities();
        if (cur.getCount() == 0) {
            Toast.makeText(MainActivity.this, "Ville Ajoutée", Toast.LENGTH_LONG).show();
            return;
        }

        String[] valeurs = new String[]{
                db.COLUMN_CITY_NAME,
                db.COLUMN_COUNTRY,
                db.COLUMN_TEMPERATURE
        };
        int[] item = new int[]{
                R.id.cName,
                R.id.cCountry,
                R.id.temperature


        };
        cursor = new SimpleCursorAdapter(this, R.layout.row, cur, valeurs, item, 0);
        Liste = (ListView) findViewById(R.id.liste);
        Liste.setAdapter(cursor);
        cursor.notifyDataSetChanged();


    }


//******************recuperer tout les elements de notre cursor dans une lise***********************

    public ArrayList<City> liste() {

        Cursor cur = db.fetchAllCities();
        City city = null;
        ArrayList<City> listeCity = new ArrayList<>();
        name = cur.getString(cur.getColumnIndex(db.COLUMN_CITY_NAME));
        country = cur.getString(cur.getColumnIndex(db.COLUMN_COUNTRY));
        temperature = cur.getString(cur.getColumnIndex(db.COLUMN_TEMPERATURE));
        humidity = cur.getString(cur.getColumnIndex(db.COLUMN_HUMIDITY));
        speed = cur.getString(cur.getColumnIndex(db.COLUMN_WIND_SPEED));
        direction = cur.getString(cur.getColumnIndex(db.COLUMN_WIND_DIRECTION));
        couldiness = cur.getString(cur.getColumnIndex(db.COLUMN_CLOUDINESS));
        icon = cur.getString(cur.getColumnIndex(db.COLUMN_ICON));
        description = cur.getString(cur.getColumnIndex(db.COLUMN_DESCRIPTION));
        update = cur.getString(cur.getColumnIndex(db.COLUMN_LAST_UPDATE));


        city = new City(name, country, temperature, humidity, speed, direction, couldiness, icon, description, update);


        listeCity.add(city);
        while (cur.moveToNext()) {

            name = cur.getString(cur.getColumnIndex(db.COLUMN_CITY_NAME));
            country = cur.getString(cur.getColumnIndex(db.COLUMN_COUNTRY));
            temperature = cur.getString(cur.getColumnIndex(db.COLUMN_TEMPERATURE));
            humidity = cur.getString(cur.getColumnIndex(db.COLUMN_HUMIDITY));
            speed = cur.getString(cur.getColumnIndex(db.COLUMN_WIND_SPEED));
            direction = cur.getString(cur.getColumnIndex(db.COLUMN_WIND_DIRECTION));
            couldiness = cur.getString(cur.getColumnIndex(db.COLUMN_CLOUDINESS));
            icon = cur.getString(cur.getColumnIndex(db.COLUMN_ICON));
            description = cur.getString(cur.getColumnIndex(db.COLUMN_DESCRIPTION));
            update = cur.getString(cur.getColumnIndex(db.COLUMN_LAST_UPDATE));


            city = new City(name, country, temperature, humidity, speed, direction, couldiness, icon, description, update);


            listeCity.add(city);
        }
        return listeCity;


    }

    public  class ExecuteTask1 extends AsyncTask<String,Void,String>{


        @Override
        protected String doInBackground(String... strings) {


            URL url;
            HttpURLConnection urlConnection=null;

            try{
               // Cursor corsor = db.fetchAllCities();
              //  while(corsor.moveToNext()){
                 //  city=db.cursorToCity(corsor);
                   url=new URL(strings[0]);
                   urlConnection=(HttpURLConnection)url.openConnection();
                    is=urlConnection.getInputStream();
                    json=new JSONResponseHandler(city);
                    json.readJsonStream(is);
              //  }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
               // Cursor corsor = db.fetchAllCities();
             //  while(corsor.moveToNext()){
                    //city=db.cursorToCity(corsor);
                    db.updateCity(city);

               // }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }



}




