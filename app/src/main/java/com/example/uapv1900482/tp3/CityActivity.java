package com.example.uapv1900482.tp3;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private EditText cityName,county1,temp,humid,wind1,cloudiness,update;
    private ImageView imageWeatherCondition;
    private City city;
    JSONResponseHandler json;
    InputStream is;
    InputStreamReader reader;
    ProgressDialog dialog;
    String name,country,speed,temperature,humidity,wind,lastUpadte,clouds,icon;
    WeatherDbHelper db;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        city = (City) getIntent().getParcelableExtra("city");
        db=new WeatherDbHelper(this);

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);







//
      updateView();












        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

          findWeather(v);
 dialog = ProgressDialog.show(CityActivity.this, "",
                        "Loading. Please wait..." );


            }
        });





    }
    public void findweather(View v){

        URL url= null;
        try {
            url = WebServiceUrl.build(city.getName(),city.getCountry());
            ExecuteTask task=new ExecuteTask();
            task.execute(url.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }

    public void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity()+" %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }


    public void findWeather(View v){



        try {
            URL url=WebServiceUrl.build(city.getName(),city.getCountry());
            ExecuteTask task=new ExecuteTask();
            task.execute(url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }







   // elle va recuperer toutes les info a partir de website
   public  class ExecuteTask extends AsyncTask<String,Void,String>{


        @Override
        protected String doInBackground(String... strings) {


            URL url;
            HttpURLConnection urlConnection=null;

            try{
                url=new URL(strings[0]);

                urlConnection=(HttpURLConnection)url.openConnection();
                 is=urlConnection.getInputStream();


                json=new JSONResponseHandler(city);
                json.readJsonStream(is);




            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                updateView();
                db.updateCity(city);


                dialog.dismiss();


            } catch (Exception e) {
                e.printStackTrace();
            }


       }
}



   
}
